'use strict';

const Url = require('../helpers/catalog/Url');

class Catalog {

  handler(request, reply) {
    if (request.pre.redirect.hasRedirect === true) {
      return reply.redirect(request.pre.redirect.url).code(request.pre.redirect.code)
    }

    let objUrl = new Url(request);
    objUrl.parse((error, result) => {
      if (error) {
        return reply.redirect('http://www.dafiti.com.br/' + request.path);
      }

      return reply.proxy({
        passThrough: true,
        xforward: true,
        uri: `${Config.proxy.address}:${Config.proxy.port}/${Config.proxy.path}?filters=${JSON.stringify(result.params)}&queryFilters=${JSON.stringify(result.query)}`
      });
    });
  }

}

module.exports = Catalog;
