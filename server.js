'use strict';

const Hapi = require('hapi');

const env = process.env.NODE_ENV;
if (env == undefined) {
  console.log("NODE_ENV not defined");
  process.exit();
}

global.Config = require('./config/config.' + env + '.json');
const Routes = require('./config/routes.js');

const server = new Hapi.Server();
server.connection({ port: 3300, routes: { cors: true }});

server.register({
    register: require('h2o2')
}, function (err) {
  server.route(Routes);
});

server.start( (err) => {
 if (err) {
    throw err
  }
  console.log(`Server running at: ${server.info.uri}`);
});
