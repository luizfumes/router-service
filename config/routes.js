'use strict';

let Catalog = require('../handlers/Catalog')
let UrlManager = require('../services/UrlManager');

let objUrlManager = new UrlManager();
let objCatalog = new Catalog();

module.exports = [
  {
    method: 'GET',
    path: '/{path*}',
    config: {
      pre: [
        [{
          method: objUrlManager.hasRedirect,
          assign: 'redirect'
        }]
      ],
      handler: objCatalog.handler
    }
  },
  {
    method: 'GET',
    path:  '/favicon.ico',
    handler: (request, reply) => {
      
    }
  }
]