const chai = require('chai');
const expect = chai.expect;

global.Config = require('../../../config/config.test.json');

const Url = require('../../../helpers/catalog/Url');

describe('CatalogUrlParser', () => {
  it('Query strings must return in key query', () => {

    const mockRequest = {
      query: 'price=64-164&heel_shape=Cone',
      params: {
        path: '/'
      }
    }

    let objUrl = new Url(mockRequest);
    objUrl.parse(result => {
      expect(result).to.have.deep.property('query', "price=64-164&heel_shape=Cone")
    })
  })
})