'use strict';

const unserialize = require('locutus/php/var/unserialize')

const Memcached = require('Memcached');
const memcached = new Memcached(`${Config.cache.uri}:${Config.cache.port}`);

class UrlManager {

  hasRedirect(request, reply) {
    
    let key = request.url.path.substring(1);
    let urlKey = Config.cache.url_manager_key.replace("#STORE_ID#", Config.stores.dafiti).replace("#KEY#", key);
    
    memcached.get(urlKey, (err, data) => {
      if (data !== undefined) {
        data = unserialize(data)
        let redirect = {
          hasRedirect: true,
          url: data.target_path,
          code: parseInt(data.http_code)
        }
        return reply(redirect);
      }
      reply({hasRedirect: false});
    });
  }

}

module.exports = UrlManager;