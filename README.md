# Router Service #

Identifies a URL and makes a proxy for the correct service

## Dev ##

Ports

* 3000
* 3030

This service uses Memcache from ALICE, before you start make sure that the container DAFITI_CACHE is running.

The first time

```
$ docker-compose build
```

Edit /etc/hosts

```
127.0.0.1 router.dafiti.dev
```

To RUN

```
# docker-compose up -d
```

The service will works in the port 3000

```
http://router.dafiti.dev:3000
```

## Live ##

Ports:

* 3000

Create a configuration file in config/ with the name config.production.json

```
$ cd config/
$ cp config.production.json.example config.production.json
```

Always after the Deploy, you must install the dependencies

```
$ npm install
```