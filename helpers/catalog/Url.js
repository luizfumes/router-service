'use strict';

const Memcached = require('Memcached');
const objMemcached = new Memcached(`${Config.cache.uri}:${Config.cache.port}`);
const unserialize = require('locutus/php/var/unserialize')

const MULTIPLE_KEY_URL = '/';
const SIZE_KEY_STRING = 'tamanho-';
const COLOR_KEY_CHAR = "_";
const MULTIPLE_KEY_PARAMS = '.';
const MULTIPLE_KEY_SIZE = '-';
const MULTIPLE_KEY_SEPARATOR = '--';
const SPECIAL_ROUTES = ['special-price', 'new-products', 'all-products'];
const GENDERS = ['femini', 'mascul', 'menino', 'menina'];

class Url {

  constructor(request, memcached) {
    this._request = request;
    this.removeSpecialRoute();
    this._memcached = (!memcached) ? objMemcached : memcached;
  }

  setMemcached(memcached) {
    this._memcached = memcached;
  }

  getMemcached() {
    return this._memcached;
  }

  removeSpecialRoute() {
    SPECIAL_ROUTES.forEach(route => {
      this._request.params.path = this._request.params.path.replace(route.concat(MULTIPLE_KEY_URL), '');
    })
  }

  getSegments(segmentCategory) {
    let segment = []
    let gender = "";

    if (segmentCategory) {
      let segmentParts = segmentCategory.split(MULTIPLE_KEY_SIZE);
      gender = segmentParts.pop().substring(0, 6);
      if (GENDERS.indexOf(gender) != -1) {
        segment.push(gender)
      }
    }

    if (this._request.query.segment) {
      let segmentQuery = this._request.query.segment.split(MULTIPLE_KEY_SEPARATOR);
      segmentQuery.forEach(seg => {
        gender = seg.substring(0, 6);
        if (GENDERS.indexOf(gender) != -1) {
          segment.push(gender)
        }
      })
    }

    return segment
  }

  getBrandsFromUrl(urlBrands, callback) {
    if (!urlBrands) {
      return callback({})
    }
    
    let arrayBrands = urlBrands.split(MULTIPLE_KEY_SEPARATOR);
    let lookupKeys = [];
    arrayBrands.forEach(brand => {
      let keyBrandName = Config.cache.brand_key.replace("#STORE_ID#", Config.stores.dafiti).replace("#BRAND#", brand);
      lookupKeys.push(keyBrandName);
    })
    this._memcached.getMulti(lookupKeys, (errBrand, brands) => {
      let filters = {};
      if (Object.keys(brands).length > 0) {
        filters.brand = [];
        filters.brandId = [];
        lookupKeys.forEach((value, i) => {
          if (brands[value] !== undefined) {
            filters.brandId.push(brands[value]);
            filters.brand.push(arrayBrands[i]);
          }
        })
      }
      callback(filters)
    });
  }

  getCategoryFromUrl(urlCategory) {
    if (!urlCategory) {
      return "";
    }

    let segmentParts = urlCategory.split(MULTIPLE_KEY_SIZE);
    return segmentParts[0];
  }

  parse(callback) {
    let filters = {};
    let mainPart = this._request.params.path.split(MULTIPLE_KEY_URL).filter(value => value);
    
    let lastElement;
    let secondLastPart;
    let paramsPart = [];

    /**
     * Verificar se a primeira parte da URL e Brand ou Category-Segment
     * Caso nao seja nenhuma dessas opcoes, reencaminha para o alice (pois pode ser CMS)
     */
    this.getBrandsFromUrl(mainPart[0], brands => {
      let isBrands = false;
      let hasGender = false;
      let segments = "";

      if (Object.keys(brands).length > 0) {
        isBrands = true;
      }
      if (!isBrands) {
        segments = this.getSegments(mainPart[0]);
        if (segments.length > 0) {
          hasGender = true;
        }
      }

      /*
       * Caso nao seja brand ou category-segment, reencaminha para o Alice
       */
      if (!isBrands && !hasGender) {
        return callback(true)
      }

      filters.category = {}

      if (hasGender) {
        filters.category = this.getCategoryFromUrl(mainPart[0])
        filters.segments = segments;
        let firstParty = mainPart.shift();
        filters.firstPart = (firstParty == undefined) ? "" : firstParty;
      }

      lastElement = mainPart.pop();
      secondLastPart = mainPart.pop();
      
      if (lastElement !== undefined)
        paramsPart = lastElement.split(MULTIPLE_KEY_PARAMS);

      filters.secondPart = (secondLastPart === undefined) ? '' : secondLastPart

      let final = [];
      paramsPart.forEach(param => {
        if (param.indexOf(SIZE_KEY_STRING) !== -1) {
          filters.size = param.substring(SIZE_KEY_STRING.length).split(MULTIPLE_KEY_SIZE);
          return;
        }
        if (param.indexOf(COLOR_KEY_CHAR) !== -1) {
          filters.color = param.split(COLOR_KEY_CHAR).filter(value => value);
          return;
        }
        filters.brand = param;
      })

      let brandNames = [];
      if (filters.brand !== undefined)
        brandNames = filters.brand.split(MULTIPLE_KEY_SEPARATOR);

      let lookupKeys = [];
      brandNames.forEach((name, i ) => {
        let keyBrandName = Config.cache.brand_key.replace("#STORE_ID#", Config.stores.dafiti).replace("#BRAND#", name);
        lookupKeys.push(keyBrandName);
      })
      
      if (lookupKeys.length == 0) {
        callback(false, {
          params: filters,
          query: this._request.query
        })
      }

      let cleanBrandKey = "";
      this._memcached.getMulti(lookupKeys, (errBrand, brand) => {
        filters.brand = [];
        filters.brandId = [];
        lookupKeys.forEach((value, i) => {
          if (brand[value] !== undefined) {
            filters.brandId.push(brand[value]);
            filters.brand.push(brandNames[i]);
          }
        })

        if (isBrands) {
          filters.brandId = filters.brandId.concat(brands.brandId)
          filters.brand = filters.brand.concat(brands.brand)
        }

        callback(false, {
          params: filters,
          query: this._request.query
        })
      })
    })
  }
}

module.exports = Url;